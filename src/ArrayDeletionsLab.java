public class ArrayDeletionsLab {
    public static void main(String[] args){
        int[] num = {1,2,3,4,5};
        int deleteIndex = 2;
        int deleteValue = 4;
        int[] newArray = new int[num.length - 1];
        for (int i = 0; i < deleteIndex; i++){
            newArray[i] = num[i];
        }
        for (int i = deleteIndex + 1; i < num.length; i++){
            newArray[i - 1] = num[i];
        }
        for (int i = 0; i < deleteValue; i++){
            if(newArray[i]==deleteValue){
                //newArray[i];
            }
        //}
        //for (int i = deleteValue + 1; i < num.length; i++){
         //   newArray[i - 1] = num[i];
        //}

        System.out.println("Original array");
        printArray(num);
        System.out.println("Array ");
        printArray(newArray);
        //System.out.println("Array after deleting element with value");
        //printArray(newArray);
    }}

    private static void printArray(int[] array) {
        for (int i = 0; i < array.length; i++){
            System.out.print(array[i] + " ");
        }
    }
}
